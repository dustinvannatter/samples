#!/bin/bash

## This example uses bash to create a deployment system for creating a skeleton repo in git and then using bash
## commands to deploy the site and setup an example CraftCMS site in a few lines of code (with the newly generated
## code being put into its own codebranch for customization as needed)

echo "";
echo "Running deployment for [skeleton] .. ";

echo "Enter the Site Code Path (ie. sitename): ";

read codepath

echo "Code Path will be [$codepath]";

echo "Enter the Site Name (ie. Site Name): ";

read sitename

echo "Site Name will be [$sitename]";

echo "Enter the Primary Color (ie. #336699): ";

read primarycolor

echo "Primary Color will be [$primarycolor]";


curl --user dustinvannatter:xxx https://api.bitbucket.org/1.0/repositories/ --data name=$codepath --data is_private='true' --data owner=dustinvannatter
git copy https://dustinvannatter:xxx@bitbucket.org/dustinvannatter/skeleton https://dustinvannatter:xxx@bitbucket.org/dustinvannatter/$codepath

ssh root@vannatter.com << EOF
  git clone git@bitbucket.org:dustinvannatter/$codepath.git /var/www/staging/$codepath ;

  echo "";
  echo "creating directories and changing permissions .. ";

  chmod -R 777 /var/www/staging/$codepath/craft/config;
  chmod -R 777 /var/www/staging/$codepath/craft/storage;
  mkdir /var/www/staging/$codepath/public/cache;
  chmod -R 777 /var/www/staging/$codepath/public/cache;

  mkdir /var/www/staging/$codepath/public/uploads;
  chmod -R 777 /var/www/staging/$codepath/public/uploads;

  echo "";
  echo "creating configuration scripts .. ";

  cp /var/www/staging/$codepath/db.txt-default /var/www/staging/$codepath/db.txt;
  cp /var/www/staging/$codepath/craft/config/db.php-default /var/www/staging/$codepath/craft/config/db.php;
  cp /var/www/staging/$codepath/craft/config/general.php-default /var/www/staging/$codepath/craft/config/general.php;
  cp /var/www/staging/$codepath/public/assets/css/helpers/_variables.scss-default /var/www/staging/$codepath/public/assets/css/helpers/_variables.scss;

  echo "";
  echo "creating configuration options .. ";

  replace "--DBPASSWORD--" "pass" -- /var/www/staging/$codepath/craft/config/db.php;
  replace "--DBNAME--" "$codepath" -- /var/www/staging/$codepath/craft/config/db.php;
  replace "--SITEPATH--" "$codepath" -- /var/www/staging/$codepath/craft/config/general.php;
  replace "--SITENAME--" "$sitename" -- /var/www/staging/$codepath/craft/config/general.php;
  replace "--PRIMARY_COLOR--" "$primarycolor" -- /var/www/staging/$codepath/public/assets/css/helpers/_variables.scss;
  replace "--SITENAME--" "$sitename" -- /var/www/staging/$codepath/db.txt;
  replace "--SITEPATH--" "$codepath" -- /var/www/staging/$codepath/db.txt;

  echo "";
  echo "creating mysql database .. ";
  mysqladmin create $codepath -pXXX;

  echo "";
  echo "importing default database .. ";
  mysql -u root -pXXX $codepath < /var/www/staging/$codepath/db.txt

  exit;
EOF

echo "Deployment finished!";
echo "";
