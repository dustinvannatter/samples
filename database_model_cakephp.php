<?

/*

	In this example, I'm showing the basics for defining a CakePHP model and the data binding that this specific
	table (products) uses.

*/

	class Product extends AppModel {
		var $name = 'Product';
		var $displayField = 'name';
		var $recursive = -1;
		var $virtualFields = array(
			'wishlist_count' => 'SELECT COUNT(*) FROM wishlists AS WishlistCount WHERE WishlistCount.product_id = Product.id',
			'hold_count' => 'SELECT COUNT(*) FROM holds AS HoldCount WHERE HoldCount.product_id = Product.id',
		);

		var $belongsTo = array(
			'Category' => array(
				'className' => 'Category',
				'foreignKey' => 'category_id',
				'conditions' => '',
				'fields' => '',
				'order' => ''
			),
			'ProductType' => array(
				'className' => 'ProductType',
				'foreignKey' => 'product_type_id',
				'conditions' => '',
				'fields' => '',
				'order' => ''
			)
		);

		var $hasMany = array(
			'ProductElement' => array(
				'className' => 'ProductElement',
				'foreignKey' => 'product_id',
				'dependent' => false,
				'conditions' => '',
				'fields' => '',
				'order' => '',
				'limit' => '',
				'offset' => '',
				'exclusive' => '',
				'finderQuery' => '',
				'counterQuery' => ''
			),
			'ProductMedia' => array(
				'className' => 'ProductMedia',
				'foreignKey' => 'product_id',
				'dependent' => false,
				'conditions' => '',
				'fields' => '',
				'order' => array('ProductMedia.width DESC'),
				'limit' => '',
				'offset' => '',
				'exclusive' => '',
				'finderQuery' => '',
				'counterQuery' => ''
			),
			'DefaultImage' => array(
					'className' => 'ProductMedia',
					'foreignKey' => 'product_id',
					'limit' => 1,
					'order' => array('DefaultImage.width DESC'),
			),
			'LatestReviews' => array(
				'className' => 'Review',
				'foreignKey' => 'product_id',
				'dependent' => false,
				'conditions' => '',
				'fields' => '',
				'order' => array('LatestReviews.modified DESC'),
				'limit' => 25,
				'offset' => '',
				'exclusive' => '',
				'finderQuery' => '',
				'counterQuery' => ''
			)
		);

		function beforeSave() {
			if(@$this->data['Product']['clean_name'] && !empty($this->data['Product']['clean_name'])) {
				$this->data['Product']['seo_name'] = parent::seoName($this->data['Product']['clean_name'], 'seo_name', 'id', @$this->data['Product']['id']);
			} elseif(@$this->data['Product']['name'] && !empty($this->data['Product']['name'])) {
				$this->data['Product']['seo_name'] = parent::seoName($this->data['Product']['name'], 'seo_name', 'id', @$this->data['Product']['id']);
			}

			if(@$this->data['Product']['clean_name']) {
				## calls getFullText() on apps/app_model.php
				$this->data['Product']['fulltext_clean_name'] = parent::getFullText($this->data['Product']['clean_name']);
			}

			if(@$this->data['Product']['name']) {
				## calls getFullText() on apps/app_model.php
				$this->data['Product']['fulltext_name'] = parent::getFullText($this->data['Product']['name']);
			}

			return true;
		}

		function nameExists($name=null, $id=null) {
			if($name) {
				if($id) {
					$prod = $this->find('first', array('conditions' => array('Product.name' => $name, 'Product.id !=' => $id), 'recursive' => -1));
				} else {
					$prod = $this->find('first', array('conditions' => array('Product.name' => $name), 'recursive' => -1));
				}

				if($prod) {
					return $prod;
				} else {
					return false;
				}
			}
		}

		function add($name=null, $category=null, $product_type=0, $added_by=0, $raw="") {
			if(!$name || !$category) {
				return false;
			}

			$product = array(
					'Product' => array(
							'name' => $name,
							'category_id' => $category,
							'product_type_id' => $product_type,
							'added_by' => $added_by,
							'raw' => serialize($raw)
					)
			);

			if(@$raw['list_price']) {
				$product['Product']['list_price'] = substr($raw['list_price'], 0, strlen($raw['list_price']) - 2) . "." . substr($raw['list_price'], strlen($raw['list_price']) - 2);
			}

			if(@$raw['amazon_price']) {
				$product['Product']['amazon_price'] = substr($raw['amazon_price'], 0, strlen($raw['amazon_price']) - 2) . "." . substr($raw['amazon_price'], strlen($raw['amazon_price']) - 2);
			}

			if(@$raw['description']) {
				$product['Product']['description'] = mysql_real_escape_string($raw['description']);
			}

			//$this->create($product);
			$this->create();
			if($this->save($product)) {
				return $this->id;
			} else {
				return false;
			}
		}

		function forceUpdate($prod_id=null, $raw=null) {
			if($prod_id && $raw) {
				$this->id = $prod_id;
				$this->saveField('raw', serialize($raw));

				$update = array();

				$prod = $this->findById($prod_id);

				if($prod['Product']['edited_by'] == 0) {
					## only update products that haven't been manually edited
					$update['Product.description'] = '"' . @mysql_real_escape_string($raw['description']) . '"';

				if(@$raw['list_price']) {
					$update['Product.list_price'] = substr($raw['list_price'], 0, strlen($raw['list_price']) - 2) . "." . substr($raw['list_price'], strlen($raw['list_price']) - 2);
				}

				if(@$raw['amazon_price']) {
					$update['Product.amazon_price'] = substr($raw['amazon_price'], 0, strlen($raw['amazon_price']) - 2) . "." . substr($raw['amazon_price'], strlen($raw['amazon_price']) - 2);
				}
				}

				$this->updateAll($update, array('Product.id' => $prod_id));
			}
		}

		function getCount() {
			$this->unbindModel(array('hasOne' => array('DefaultImage')));
			return $this->find('count');
		}

		function getList($page=1, $limit=20) {
			$this->bindModel(array('hasOne' => array('ActiveProduct', 'FocusDeal')));
			$this->unbindModel(array('hasOne' => array('DefaultImage')));
			return $this->find('all', array('recursive' => 1, 'page' => $page, 'limit' => $limit));
		}

		function findByName($product_name) {
			return $this->find('first', array('conditions' => array('Product.name' => $product_name)));
		}

		function findById($product_id, $recursive=-1) {
			return $this->find('first', array('conditions' => array('Product.id' => $product_id), 'recursive' => $recursive));
		}

		function getById($product_id, $recursive=-1) {
			return $this->findById($product_id, $recursive);
		}

		function findByArray($args, $recursive=-1) {
			return $this->find('all', array('conditions' => $args, 'recursive' => $recursive));
		}

		function remove($product_id) {
			$this->delete($product_id);
			return true;
		}

		function toggle($product_id) {
			$this->bindModel(array('hasOne' => array('ActiveProduct')));

			if($data = $this->ActiveProduct->getByProductId($product_id)) {
				$this->ActiveProduct->delete($data['ActiveProduct']['id']);
			} else {
				$this->ActiveProduct->save(array('ActiveProduct' => array('product_id' => $product_id)));
			}
		}
	}
?>