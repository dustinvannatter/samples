//
// This example is using the Laravel Elixir process to load js / css dependencies through gulp / bower.
// See https://laravel.com/docs/5.3/elixir for extended info
//

var elixir = require('laravel-elixir');
var bowerDir = './resources/assets/bower/';

elixir(function (mix) {
	mix.scripts([
		'jquery/dist/jquery.min.js',
		'bootstrap/dist/js/bootstrap.min.js'
	], 'public/js/all.js', bowerDir);

	mix.less('main.less')
		.less('./resources/assets/bower/bootstrap/less/bootstrap.less', 'public/css/bootstrap.css');
});