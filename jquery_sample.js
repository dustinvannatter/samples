// This is an example of jQuery wrote to handle user interaction on a preference page of a site I did for a client.
// This code interacts with some AJAX calls to parse response data and/or send update data.

$(document).ready(function() {
	
	$('.hlpr').click(function() {
		$(this).fadeOut();
	});
	
	$('.add_btn').live('click', function() {
		var i = $(this).attr('data-cctid');
		
		if ($('#ion_box_'+i).is(":visible")) {
			$('#ion_box_'+i).fadeOut();
		} else {
			$('#ion_box_'+i).fadeIn();
		}
	});
	
	$('.c_expand').click(function() {		
		var i = $(this).attr('data-cctid');

		if ($('#comm_'+i).is(":visible")) {
			$(this).parent().removeClass('cp_expanded');
			$('#comm_'+i).hide();
			$('#expand_'+i).text('+');
			$('#row_'+i).removeClass('cp_expanded_border');
		} else {
			$(this).parent().addClass('cp_expanded');
			$('#comm_'+i).show();
			$('#expand_'+i).text('-');
			$('#row_'+i).addClass('cp_expanded_border');
		}
	});
	
	
	function delRow(i) {
		$('#tr_chi_'+i).remove();
	}
	
	$('.del_chi').live('click', function() {
		var cctid = $(this).attr('data-cctid');
		var i = $(this).attr('data-chiid');
		if (i) {
			
			$.ajax({
				url: "/ajax/clickhistorydel?id=" + i,
				success: function(data) {
					
					if(data == "1") {
						delRow(i);
						
						var tot_rows = $('#comc_tbl_'+cctid+' tr').length;
						var chi_rows = $('#comc_tbl_'+cctid+' tr.tr_chi').length;

						if (tot_rows == 1) {
							$('#expand_'+cctid).parent().removeClass('cp_expanded');
							$('#comm_'+cctid).hide();
							$('#expand_'+cctid).hide();
							$('#row_'+cctid).removeClass('cp_expanded_border');
						}
						
						if (chi_rows == 0) {
							if (tot_rows > 1) {
								$('#status_'+cctid).html("<img src='http://www.couponcactuscdn.com/images/buttons/validate_ok.png' alt='Cash back awarded!' title='Cash back awarded!' />");
							} else {
								$('#status_'+cctid).html("<img src='http://www.couponcactuscdn.com/images/clear.gif' width='17' height='16' />");
							}
						}
					} 
				}
			});
		}
	});
	
	
	$('.ion').bind('keypress', function(e) {
		var code = (e.keyCode ? e.keyCode : e.which);
		 if(code == 13) {
			var i = $(this).attr('data-cctid');
			var v = $(this).val();
			var n = $('#ionn_'+i).val();
			
			$.ajax({
				url: "/ajax/clickhistoryflag?cctid=" + i + "&v=" + escape(v) + "&n=" + escape(n),
				success: function(data) {
					if(data == "0") {
					} else {
						var ele = data.split(",");
						
						$('#status_'+i).html("<img src='http://www.couponcactuscdn.com/images/warning.png' alt='Investigating' title='Investigating' />");
						$('#ion_box_'+i).fadeOut();
						$('#comc_tbl_'+i+' tr:last').after('<tr id="tr_chi_' + ele[0] + '" class="tr_chi"><td align="center">' + ele[1] + '</td><td align="center"><nobr/>' + v + ' <span data-cctid="' + i + '" data-chiid="' + ele[0] + '" id="del_chi_' + ele[0] + '" class="del_chi">[delete]</span></td><td align="left" colspan="2"><span class="notes">Notes:</span> ' + n + '</td><td align="center"><img src="http://www.couponcactuscdn.com/images/warning.png" title="Investigating" /></td></tr>');

						$('#expand_'+i).parent().addClass('cp_expanded');
						$('#comm_'+i).show();
						$('#expand_'+i).show().text('-');
						$('#row_'+i).addClass('cp_expanded_border');
						
						$('#ion_'+i).val("");
						$('#ionn_'+i).val("");
					}
				}
			});
		 }		
	});

	$('.ionn').bind('keypress', function(e) {
		var code = (e.keyCode ? e.keyCode : e.which);
		 if(code == 13) {
			var i = $(this).attr('data-cctid');
			var v = $('#ion_'+i).val();
			var n = $(this).val();
			
			$.ajax({
				url: "/ajax/clickhistoryflag?cctid=" + i + "&v=" + escape(v) + "&n=" + escape(n),
				success: function(data) {
					if(data == "0") {
					} else {
						var ele = data.split(",");
						
						$('#status_'+i).html("<img src='http://www.couponcactuscdn.com/images/warning.png' alt='Investigating' title='Investigating' />");
						$('#ion_box_'+i).fadeOut();
						$('#comc_tbl_'+i+' tr:last').after('<tr id="tr_chi_' + ele[0] + '" class="tr_chi"><td align="center">' + ele[1] + '</td><td align="center"><nobr/>' + v + ' <span data-cctid="' + i + '" data-chiid="' + ele[0] + '" id="del_chi_' + ele[0] + '" class="del_chi">[delete]</span></td><td align="left" colspan="2"><span class="notes">Notes:</span> ' + n + '</td><td align="center"><img src="http://www.couponcactuscdn.com/images/warning.png" title="Investigating" /></td></tr>');

						$('#expand_'+i).parent().addClass('cp_expanded');
						$('#comm_'+i).show();
						$('#expand_'+i).show().text('-');
						$('#row_'+i).addClass('cp_expanded_border');
						
						$('#ion_'+i).val("");
						$('#ionn_'+i).val("");
					}
				}
			});
		 }		
	});
	
	$('.ionb').click(function() {

		var i = $(this).attr('data-cctid');
		var v = $('#ion_'+i).val();
		var n = $('#ionn_'+i).val();

		$.ajax({
			url: "/ajax/clickhistoryflag?cctid=" + i + "&v=" + escape(v) + "&n=" + escape(n),
			success: function(data) {
				if(data == "0") {
				} else {
					var ele = data.split(",");
					
					$('#status_'+i).html("<img src='http://www.couponcactuscdn.com/images/warning.png' alt='Investigating' title='Investigating' />");
					$('#ion_box_'+i).fadeOut();
					$('#comc_tbl_'+i+' tr:last').after('<tr id="tr_chi_' + ele[0] + '" class="tr_chi"><td align="center">' + ele[1] + '</td><td align="center"><nobr/>' + v + ' <span data-cctid="' + i + '" data-chiid="' + ele[0] + '" id="del_chi_' + ele[0] + '" class="del_chi">[delete]</span></td><td align="left" colspan="2"><span class="notes">Notes:</span> ' + n + '</td><td align="center"><img src="http://www.couponcactuscdn.com/images/warning.png" title="Investigating" /></td></tr>');

					$('#expand_'+i).parent().addClass('cp_expanded');
					$('#comm_'+i).show();
					$('#expand_'+i).show().text('-');
					$('#row_'+i).addClass('cp_expanded_border');
					
					$('#ion_'+i).val("");
					$('#ionn_'+i).val("");
				}
			}
		});
	});

});