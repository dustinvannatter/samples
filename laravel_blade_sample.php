{{-- This blade example is showing the basics of laravel blade templating and how you extend layouts with section blocks --}}
{{-- In this example, we are showing the main content block which we are loading with an example job posting --}}

@extends('layouts.default')

@section('title', $job->title)

@section('content')

	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<h2>{{ $job->company->name }}</h2>
			</div>
			<div class="col-md-6">
				<ul class="list-unstyled">
					<li>{{ $job->status->name }}</li>
					<li>{{ $job->jobtype->name }}</li>
				</ul>

				<p>{{ $job->title }}</p>
				<blockquote>
					{{ nl2br($job->description) }}
				</blockquote>
			</div>

			<div class="col-md-6">
				<ul class="list-unstyled">
					<li>{{ $job->location_street }}</li>
					@if (!empty($job->location_street2)) <li>{{ $job->location_street2 }}</li> @endif
					<li>{{ $job->city_state }}</li>
					<li>{{ $job->location_zip }}</li>
				</ul>
			</div>

			@if ($job->keywords)
				<div class="col-md-12">
					@foreach ($job->keywords as $word)
						<span class="keyword">{{ $word->keyword->name }}</span>
					@endforeach
				</div>
			@endif
		</div>
	</div>
@endsection