<?php

// This is a controller for a website a friend and I have been developing for fun (a job posting / tracking site that
// aggregates job postings on different job boards and finds common keyword density)
// This is built extending on Laravel's auth model and then implements several controller functions for the admin tool


namespace App\Http\Controllers;

use App\Http\Requests\AdminAddCompanyRequest;
use App\Http\Requests\AdminAddTagRequest;
use App\Http\Requests\AdminAddUserRequest;
use App\Http\Requests\AdminJobAddRequest;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Auth;
use DB;

use App\Company;
use App\Job;
use App\User;
use App\Status;
use App\JobType;
use App\JobKeyword;
use App\Keyword;

/**
 * Class AdminController
 * @package App\Http\Controllers
 */
class AdminController extends Controller
{
	/**
	 * @return $this
	 */
	public function index()
	{
		$totalUsers = User::count();
		$totalJobs = Job::count();
		$totalCompanies = Company::count();

		return view('admin.index')->with([
						'totalUsers' => $totalUsers,
						'totalJobs' => $totalJobs,
						'totalCompanies' => $totalCompanies
				]
		);
	}

	/**
	 * @return $this
	 */
	public function getJobs()
	{
		$jobs = Job::orderBy('title')->paginate(10);

		return view('admin.jobs.index')->with('jobs', $jobs);
	}

	/**
	 * @return $this
	 */
	public function getJobAdd()
	{
		## get a list of companies
		$companies = Company::lists('name', 'id');
		$statuses = Status::lists('name', 'id');
		$jobTypes = JobType::lists('name', 'id');

		return view('admin.jobs.add')->with(['companies' => $companies, 'statuses' => $statuses, 'jobTypes' => $jobTypes]);
	}

	/**
	 * @param AdminJobAddRequest $request
	 * @return \Illuminate\Http\RedirectResponse
	 */
	public function postJobAdd(AdminJobAddRequest $request)
	{
		$data = $request->only(Job::$createFields);
		$data['user_id'] = Auth::user()->id;

		Job::create($data);

		return \Redirect::route('admin.jobs')->with('success', 'Job created successfully!');
	}

	/**
	 * @param $id
	 */
	public function getJobEdit($id)
	{
		$job = Job::findOrFail($id);

		## get a list of companies
		$companies = Company::lists('name', 'id');
		$statuses = Status::lists('name', 'id');
		$jobTypes = JobType::lists('name', 'id');

		return view('admin.jobs.edit')->with([
				'job' => $job,
				'companies' => $companies,
				'statuses' => $statuses,
				'jobTypes' => $jobTypes
		]);
	}

	public function postJobEdit($id, AdminJobAddRequest $request)
	{
		$job = Job::findOrFail($id);

		$job->update([
				'id' => $id,
				'company_id' => $request->get('company_id'),
				'user_id' => Auth::user()->id,
				'status_id' => $request->get('status_id'),
				'title' => $request->get('title'),
				'description' => $request->get('description'),
				'job_type_id' => $request->get('job_type_id'),
				'location_street' => $request->get('location_street'),
				'location_street2' => $request->get('location_street2'),
				'location_city' => $request->get('location_city'),
				'location_state' => $request->get('location_state'),
				'location_country' => $request->get('location_country'),
				'location_lat' => $request->get('location_lat'),
				'location_long' => $request->get('location_long')
		]);

		return \Redirect::route('admin.jobs')->with('success', 'Job updated!');
	}

	public function getJobKeywords($id)
	{
		$job = Job::findOrFail($id);

		$job->description = preg_replace('([a-zA-Z.,!?0-9]+(?![^<]*>))', '<span class="keyword">$0</span>', $job->description);

		$keywords = "";
		foreach ($job->keywords as $kw) {
			$keywords .= $kw->keyword->name . ",";
		}
		$keywords = rtrim($keywords, ",");

		return view('admin.jobs.keywords')->with(['job' => $job, 'keywords' => $keywords]);
	}

	public function postJobKeywords($id, Request $request)
	{
		// keywords to an array
		$keywords = explode(",", $request->get('keywords'));

		// remove previous keywords
		JobKeyword::where('job_id', $id)->delete();

		// loop and see if we have each keyword already, if we don't add it...
		foreach ($keywords as $word) {
			$wordId = 0;

			if (!Keyword::where('name', $word)->exists()) {
				// add the new keyword
				$kw = new Keyword;

				$kw->name = $word;

				$kw->save();

				$wordId = $kw->id;
			} else {
				// get the id of the current keyword
				$kw = Keyword::where('name', $word)->first();

				$wordId = $kw->id;
			}

			$jkw = new JobKeyword;

			$jkw->job_id = $id;
			$jkw->keyword_id = $wordId;

			$jkw->save();
		}

		return \Redirect::route('admin.jobs')->with('success', 'Keywords saved!');
	}

	/**
	 * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
	 */
	public function getUsers()
	{
		$users = User::orderBy('username')->paginate(10);

		return view('admin.users.index')->with('users', $users);
		//return view('admin.users.index');
	}

	/**
	 * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
	 */
	public function getUserAdd()
	{
		return view('admin.users.add');
	}

	/**
	 * @param AdminAddUserRequest $request
	 */
	public function postUserAdd(AdminAddUserRequest $request)
	{
		//
	}
	

	public function postUserEdit($id, AdminAddUserRequest $request)
	{
		$user = User::findOrFail($id);

		$user->update([
				'id' => $id,
				'username' => $request->get('username'),
				'email' => $request->get('email'),
				'role' => $request->get('role'),
				'first_name' => $request->get('first_name'),
				'last_name' => $request->get('last_name')
		]);

		if ($request->get('change_password')) {
			$user->update(['id' => $id, 'password' => bcrypt($request->get('change_password'))]);			
		}

		return \Redirect::route('admin.users')->with('success', 'User updated!');
	}	
	
	
	/**
	 * @param $id
	 */
	public function getUserEdit($id)
	{
		$user = User::findOrFail($id);
		return view('admin.users.edit')->with(['user' => $user]);
	}	

	/**
	 * @return $this
	 */
	public function getCompanies()
	{
		$companies = Company::orderBy('name')->paginate(10);

		return view('admin.companies.index')->with('companies', $companies);
	}

	/**
	 * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
	 */
	public function getCompanyAdd()
	{
		return view('admin.companies.add');
	}

	/**
	 * @param AdminAddCompanyRequest $request
	 * @return \Illuminate\Http\RedirectResponse
	 */
	public function postCompanyAdd(AdminAddCompanyRequest $request)
	{
		Company::create($request->only(Company::$createFields));

		return \Redirect::route('admin.companies')->with('success', 'Company created successfully!');
	}

	/**
	 * @param $id
	 * @return $this
	 */
	public function getCompanyEdit($id)
	{
		$company = Company::findOrFail($id);

		return view('admin.companies.edit')->with('company', $company);
	}

	/**
	 * @param $id
	 * @param AdminAddCompanyRequest $request
	 * @return \Illuminate\Http\RedirectResponse
	 */
	public function postCompanyEdit($id, AdminAddCompanyRequest $request)
	{
		$company = Company::findOrFail($id);

		$company->update([
				'id' => $id,
				'name' => $request->get('name'),
				'url' => $request->get('url'),
				'description' => $request->get('description')
		]);

		return \Redirect::route('admin.companies')->with('success', 'Company updated!');
	}

	/**
	 * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
	 */
	public function getTags()
	{
		return view('admin.tags.index');
	}

	/**
	 * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
	 */
	public function getTagAdd()
	{
		return view('admin.tags.add');
	}

	/**
	 * @param AdminAddTagRequest $request
	 */
	public function postTagAdd(AdminAddTagRequest $request)
	{
		//
	}

	public function getKeywords()
	{
		$keywords = Keyword::join('job_keyword', 'job_keyword.keyword_id', '=', 'keywords.id')
				->groupBy('keywords.name')
				->orderBy('times', 'DESC')
				->paginate(10, ['keywords.id', 'keywords.name', DB::raw('count(*) as times')]);

		return view('admin.keywords.index')->with('keywords', $keywords);
	}
}
