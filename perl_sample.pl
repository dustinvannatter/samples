#!/usr/bin/perl

## This is a simple example of a perl script I use on my development/deployment server to automatically handle
## postback hooks from Mercurial to update our dev site

print "Content-type: text/html\n\n";
print "starting.. <br/>";

$c1 = `cd /home/purchaso.dev; ls -lsa`;
$c2 = `cd /home/purchaso.dev; /usr/bin/hg pull 2>&1`;
$c3 = `cd /home/purchaso.dev; /usr/bin/hg update 2>&1`;
print "Finished update.";

sendEmail("dustin\@vannatter.com", "dustin\@vannatter.com", "[PURCHASO UPDATE]", "Updated successfully");
sub sendEmail
{
  	my ($to, $from, $subject, $message) = @_;
        my $sendmail = '/usr/lib/sendmail';
        open(MAIL, "|$sendmail -oi -t");
                print MAIL "From: $from\n";
                print MAIL "To: $to\n";
                print MAIL "Subject: $subject\n\n";
                print MAIL "$message\n";
        close(MAIL);
}
