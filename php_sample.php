<? 

	/**
	 * This is a simple example of PHP using a custom DB connector running as a cron job to scan a directory for images (store logos)
	 * and then to create/recreate a set of 'facebook friendly' thumbnails to the scale recommended by Facebook OpenGraph (90x90)
	 * @author dustinvannatter
	 */

	session_start();
	include_once('../includes/auth.php');
	include_once "../includes/DB_new.php";
	$db = new DB();
	
	$dir = "/home/cdn/public_html/images/storelogo";
	$dest = "/home/cdn/public_html/images/fblogo";
	$fil = scandir($dir);
	$cnt = 0;
	
 	foreach ($fil as $f) {
		if (substr($f, 0, 1) != '.') {
			
			$fname = substr($f, 0, -4);
			list($width, $height, $type, $attr) = getimagesize($dir."/".$f);
			
			echo $f . " -- ";
			echo "Image width: " .$width;
			echo " - ";
			echo "Image height: " .$height;
			echo " - ";
			echo "Image type: " .$type;
			echo " - ";
			echo "Attribute: " .$attr;
			echo "<br/>";
			
			if ($type == 1) {
				$src = imagecreatefromgif($dir."/".$f);				
			} elseif ($type == 2) {
				$src = imagecreatefromjpeg($dir."/".$f);				
			} else {
				$src = imagecreatefrompng($dir."/".$f);				
			}
			
			$z = $width / 90;
			$y = round($height / $z);
			$top = round((90 - $y) / 2);
			
			## create new 90x90 from the width of this item.. 
			$im = imagecreatetruecolor(90, 90);
			$bg = imagecolorallocate ( $im, 255, 255, 255 );
			imagefill ( $im, 0, 0, $bg );
			imagecopyresampled($im, $src, 0, 0, 0, 0, 90, $y, $width, $height);			
			imagepng($im, $dest."/".$fname.".png", 5);
			$cnt++;
		}
	}

	if ($cnt > 0) {
		$sql_cron = "INSERT INTO cron (cron_job_id, result, run_time) VALUES (4, '" . $cnt . "', '" . date("Y-m-d H:i:s") . "')";
		$db->ExecuteQuery($sql_cron);
	}	
	echo "<br><br>";
	echo " done processing facebook logos! <br/>";
	echo "<br><br>";
		
?>