<?

/**
 * 
 * This is an example of PHP using the Zend Framework
 * This is a controller file for member signups on a website I worked on for a client
 * @author dustinvannatter
 *
 */

class SignupController extends Zend_Controller_Action {

	public function init() {
		CC_Controller_Plugin_Caching::$doNotCache = true; 
	}

	public function ajaxcheckemailAction() {
		$params = $this->_request->getParams();
		if (!$params['email']) {
			echo "-1";
			exit;
		} else {
			$model_user = new Model_DbTable_Member();
			$user = $model_user->ajaxGetUserByEmail($params['email']);
			
			if (@$user['mem_id']) { 
				echo "1"; 
			} else {
				echo "0";
			}
			exit;
		}
    }

	public function ajaxcheckreferralAction() {
		$params = $this->_request->getParams();
		if (!$params['mem_id']) {
				echo "-1";
				exit;
		} else {
			$model_user = new Model_DbTable_Member();
			$user = $model_user->ajaxGetUserInfo($params['mem_id']);
			
			if (@$user['mem_id']) { 
				echo "1"; 
			} else {
				echo "0";
			}
			exit;
		}
	}
    
	public function ajaxcheckpromoAction() {
		$params = $this->_request->getParams();
		if (!$params['code']) {
				echo "-1"; ## not found
				exit;
		} else {
			$model_promo = new Model_DbTable_Promo();
			$promo = $model_promo->ajaxGetPromo($params['code']);
			
			if (@$promo['id']) { 
				## we have a code, lets make sure its still active.
				if (@$promo['status'] == 1) {
					## now check the date range to make sure we are within the accepted range..
					$start_date = strtotime($promo['start_date']);
					$end_date = strtotime($promo['end_date']);
					$now_date = strtotime(date("Y-m-d"));
					
					if ( ($now_date >= $start_date) && ($now_date <= $end_date) ) {
						## now check to make sure this isnt a type 2 (limited), and if it is, we need to 
						## make sure we have some remaining..
						if ($promo['promo_type'] == 2) {
							if ($promo['remaining'] > 0) {
								## ok, it's valid!
								echo "1";
							} else {
								## this code is out of charges!
								echo "-2";
							}
						} else {
							## ok, it's valid!
							echo "1"; 
						}
					} else {
						echo "0"; ## not active
					}
				} else {
					echo "0"; ## not active					
				}
			} else {
				echo "-1"; ## not found
			}
			exit;
		}
	}    
    
	public function reverifyAction() {
		$this->view->title = "Sign Up Re-verification";
		$this->view->headTitle("Coupon Cactus Registration Resend Verification", 'SET');
		$params = $this->_request->getParams();
		
		if (!$params['id']) {
			$this->view->error_message = "Member ID is a required field!";
		}
		
		## get info about this user..
		$model_user = new Model_DbTable_Member();
		$user = $model_user->getUserInfo($params['id']);
		
		if (!$user['mem_id']) {
			$this->view->error_mesage = "Member not found!";
		} else {
			if ($user['status'] == "U") {
				## now send verification email
				$email_data = array('data'=>$user,
					'subject'=>'Verify Your Coupon Cactus Account',
                    'to'=>array('email'=>$user['email'], 'name'=>$user['name']),
                    'from'=>array('email'=>'customerservice@couponcactus.com', 'name'=>'Coupon Cactus'),
                    'url'=>'http://www.couponcactus.com', 
                  	'user_id'=>$user['mem_id'],
				    'verify' => $user['verification_code'],
                    'email'=>$user['email'],
                    'password'=>$user['password']);
				CC_SendEmail::send($email_data, 'signup/verify.phtml');
                    				
				## now redirect to /login with reverified message.
                $this->_helper->flashMessenger('member_reverified_account');
                $this->_redirect("/login/");
                exit;
			} else {
				$this->view->error_mesage = "Member already activated!";
			}
		}
	}

	public function verifyAction() {
		$this->view->title = "Sign Up Verification";
		$this->view->headTitle("Coupon Cactus Registration Verification", 'SET');
		$params = $this->_request->getParams();
		
		if (!$params['id']) {
			$this->view->error_message = "Member ID is a required field";
		} elseif (!$params['verify']) {
			$this->view->error_message = "Verification Code is a required field";
		}

		## get info about this user..
		$model_user = new Model_DbTable_Member();
		$user = $model_user->getUserInfo($params['id']);
		
		if (!$user['mem_id']) {
			$this->view->error_mesage = "Member not found!";
		} else {
			if ($user['status'] == "U") {
				if ($params['verify'] == $user['verification_code']) {
					## looks valid, go ahead and set it as active
					$activate = $model_user->activateUser($user['mem_id']);
					
					## now send welcome email
                    $email_data = array('data'=>$user,
                    			'subject'=>'Welcome to Coupon Cactus!',
                    			'to'=>array('email'=>$user['email'], 'name'=>$user['name']),
                    			'from'=>array('email'=>'customerservice@couponcactus.com', 'name'=>'Coupon Cactus'),
                    			'url'=>'http://www.couponcactus.com', 
                    			'user_id'=>$user['mem_id'],
                    			'email'=>$user['email'],
                    			'password'=>$user['password']);
                    CC_SendEmail::send($email_data, 'signup/user.phtml');
                    					
					## now redirect to /login with welcome message.
	                $this->_helper->flashMessenger('member_verified_account');
	                $this->_redirect("/login/");
                    exit;
                    
				} else {
					$this->view->error_mesage = "Invalid verification code - check it and try again!";
				}
			} else {
				$this->view->error_mesage = "Member already activated!";
			}
		}
	}

	public function thankYouAction() {
		$this->view->title = "";
		$this->view->headTitle("Coupon Cactus - Thank You", 'SET');
		$user_obj = new Model_DbTable_Member();

		$csid = $_COOKIE['csid'];
		if ($csid) {
			$user = $user_obj->find((int)$csid);
			$user = $user->current();
			$this->view->notify_clearsaleing = $user;
			$tiering_obj = new Model_DbTable_Tiering();
			$this->view->hear_from = $tiering_obj->getParent($user->mem_id);
			setcookie("csid", "");
		}          
		$this->view->headLink()->appendStylesheet('http://' . $this->view->cdn . '/css/pages/signup.css');
	}
    
	public function indexAction() {
		if (Zend_Auth::getInstance()->hasIdentity()) {
			$this->_redirect("/");
		}
    	
		$this->view->title = "";
		$this->view->headTitle("Coupon Cactus Registration", 'SET');
		$this->view->meta_description = "Coupon Cactus free membership signup page.";

		$this->view->headScript()->appendFile('http://' . $this->view->cdn . '/js/pages/signup.js', 'text/javascript');
		$this->view->headLink()->appendStylesheet('http://' . $this->view->cdn . '/css/pages/signup.css');
		
		$request = $this->getRequest();

		$form = new Form_Signup();
		$this->view->form = $form;        

		if ($this->getRequest()->isPost()) {
			$formData = $this->getRequest()->getPost();
			if ($form->isValid($formData)) {
				$verify = $this->_randstr(30);
				$db = Zend_Db_Table::getDefaultAdapter();
				$db->beginTransaction();
				try {
					$model_user = new Model_DbTable_Member();
					$model_tiering = new Model_DbTable_Tiering();
					$model_invites = new Model_DbTable_Invities();
                    
					$user_id = $model_user->addUser($form->getValues(), $verify);

					// check for promo code, if applied, we need to apply the credit..
					$promo_check = $model_user->find($user_id);
					if (@$promo_check->current()->promo_code) {
						$promo_obj = new Model_DbTable_Promo;
						$promo = $promo_obj->fetchRow($promo_obj->select()				
											->setIntegrityCheck(false)
											->from(array('p'=>'promos'))
											->where('code = ?', $promo_check->current()->promo_code));
										                    		
						$model_comm = new Model_DbTable_Commission();
						$comm_id = $model_comm->insert(array('mem_id'=>$user_id,
							'user_type'=>'Member',
							'network_id'=>$coupon->network_id,
							'get_id'=>'CC',
							'sale_amount'=>0,
							'commission_amount'=>$promo['action_val'],
							'cbp'=>0,
							'cbd'=>0,
							'trans_date'=>date( 'Y-m-d H:i:s'),
							'network_id'=>0,
							'store_id'=>0,
							'store_name'=>'Coupon Cactus',
							'coupon_id'=>0,
							'added_date'=>date( 'Y-m-d H:i:s'),
							'order_id'=>$promo['title'],
							'fcr'=>0));
						$resu = $model_user->setPromoProcessed($user_id);
					}
                    
					if ($form->getValue('didanymore')) {
						$ref_user = $model_user->find($form->getValue('didanymore'));
					} else {
						$ref_user = $model_invites->getInviteByEmail($form->getValue('email'));
					}

					if ($ref_user == null) {
						$ref_member_id = false;
					} else {
						$ref_member_id = $ref_user->current()->mem_id;
					}

					if ($ref_member_id) {
						$model_tiering->setTiering($ref_member_id, $user_id);
					}

					// unset session ref id
					$ref_session_namespace = new Zend_Session_Namespace('referral_id');
					unset($ref_session_namespace->referral_id); 
                    	
					// emails	
					// user email
					$email_data = array('data'=>$form->getValues(),
                    			'subject'=>'Welcome to Coupon Cactus!',
                    			'to'=>array('email'=>$form->getValue('email'), 'name'=>$form->getValue('first_name')." ".$form->getValue('last_name')),
                    			'from'=>array('email'=>'customerservice@couponcactus.com', 'name'=>'Coupon Cactus'),
                    			'url'=>'http://www.couponcactus.com', 
                    			'user_id'=>$user_id,
                    			'verify' => $verify,
                    			'email'=>$form->getValue('email'),
                    			'password'=>$form->getValue('password'));
					CC_SendEmail::send($email_data, 'signup/user.phtml');
					$db->commit();
				} catch (Exception $e) {
					$db->rollBack();
				}

				setcookie("csid", $user_id);

				if (!CC_ActLogin::act_login($form->getValue('email'), $form->getValue('password'), 0) ) {
					$this->_redirect("/signup/thank-you");
				} else {
					$this->_redirect("/signup/thank-you");
				}
			} else {
				$form->populate($formData);
			}
		}
	}
	
	public function lightboxAction() {
		$this->_helper->layout()->disableLayout();
	}
  
}

?>