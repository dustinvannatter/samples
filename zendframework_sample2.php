<?

/**
 * 
 * This is another example of PHP in the Zend Framework
 * This is an example of a Model file for defining database table functions
 * @author dustinvannatter
 *
 */

class Model_DbTable_CouponGroups extends Zend_Db_Table_Abstract {
	
	protected $_name = 'coupon_groups';
	
	public function getByURL($url) {
		$search_query_part = "friendly_url_name = '" . $url . "'";
		$sql = $this->select()
				->setIntegrityCheck(false)
				->from(array('cg'=>$this->_name), array("cg.id", "cg.status_id", "cg.page_title", "cg.title", "cg.friendly_url_name", "cg.description", "cg.meta_description", "cg.meta_keywords"))
				->where(new Zend_Db_Expr($search_query_part));
		$data = $this->fetchRow($sql);
		return $data;
	}
	
	public function getTabs($group_id) {
		$search_query_part = "coupon_group_id = '" . $group_id . "'";
		$sql = $this->getAdapter()->select()
				->from(array('cgt'=> 'coupon_group_tabs'), array("cgt.id", "cgt.coupon_group_id", "cgt.title", "cgt.order"))
				->where(new Zend_Db_Expr($search_query_part))
				->order('cgt.order ASC');
		$data = $this->getAdapter()->fetchAll($sql);
		return $data;
	}

	public function getCoupons($tab_id) {
		$search_query_part = "coupon_group_tab_id = '" . $tab_id . "'";
		$today = date("Y-m-d");

		$sql = $this->getAdapter()->select()
				->from(array('ct'=> 'coupon_tabs'), array("ct.id", "ct.coupon_id", "ct.order"))
				->join(array('c'=>'coupons'), 
						'ct.coupon_id = c.coupon_id', 
						array("date_format(c.exp_date,'%m/%d/%y') as exp_date", "UNIX_TIMESTAMP(c.exp_date) as exp_date_timestamp", "c.coupon_code", "c.coupon_desc", "c.couponname", "c.html_code", "c.coupon_id","c.coupon_type", "c.sort_order", 's.cashback_percent', 's.cashback_dollar',"c.added_date","c.start_date", "c.is_exclusive"))
				->join(array('s'=>'stores'), 
						's.store_id = c.store_id', 
						array('s.storename','cashback_r','store_logo','store_id'))
				->where(new Zend_Db_Expr($search_query_part))
				->where("c.start_date <= '" . $today . "' and (c.exp_date >= '" . $today . "' or c.exp_date='0000-00-00')")
				->where("c.status = 'A'")
				->where("s.status = 'A'")
				->order('ct.order ASC');
		$data = $this->getAdapter()->fetchAll($sql);
		return $data;
	}
	
}

?>